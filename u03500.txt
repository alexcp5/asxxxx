2010_10_31 Version 5.00 Update 3

   This  update  for  Version  5.00  of the  ASxxxx Cross
Assemblers rolls up updates 1 and 2 with the addition of
three new assemblers and fixes:

   (1)  New cross assemblers for STMicroelectronics
        ST6, ST7, and STM8 microprocessors.

   (2)  An ASlink list file update error fix (-u option)
        causing some errors not to be inserted into the
        created .rst file.

   (3)  An additional ASxxxx assembler option (-v) which
        enables checking for out of range signed / unsigned
        values in symbol equates and arithmetic operations.
        This option has some ambiguities as internally the
        assemblers use unsigned arithmetic for calculations.
        (e.g. for a 2-byte machine -32768 and 32768 are both
        represented as 0x8000)



2010_04_01 Version 5.00 Update 2 Items

   (1)  When using the assembler directive .end to specify
        the code entry address the assembler fails to set
        the variable .__.END. as a global.  Therefor the
        value of .__.END. is not passed to the linker and
        the start address frame is always zero.

   (2)  The linker will fail to create a start address frame
        when there is no code generated within the area/bank
        referenced by the .__.END. variable.



2010_03_03 Version 5.00 Update 1 Items

   (1)  The newest versions of gcc (and perhaps other
        compilers) give warnings about missing arguments
        in the fprintf() function. The warnings occur in:

            aslist.c      lines 679, 688, and 1015

            asout.c       line  1127

            lklist.c      lines 299, 302, 463, and 473

        This update replaces:

            fprintf(arg1, arg2)

        with

            fprintf(arg1, "%s", arg2)

        in each line of code.

   (2)  The newest versions of gcc (and perhaps other
        compilers) have defined 'getline' as a standard
        function in 'stdio.h'.  This conflicts with the
        function 'getline()' in the ASxxxx package.
        All references to 'getline()' have been changed
        to 'nxtline()'.




   Merge  the  update asxv5pxx directory and subdirectories with
the V5.00 distribution.  The following files  will  be  added  /
overwritten:  

        [asxv5pxx\asst6]
              st6adr.c
              st6pst.c
              st6mch.c
              st6.h
              st6gbl.asm
              st6err.asm
              st6err.bat
              tst6.asm
              tst6.bat

        [asxv5pxx\asst7]
              st7adr.c
              st7pst.c
              st7mch.c
              st7.h
              st7gbl.asm
              st7err.asm
              st7err.bat
              tst7.asm
              tst7.bat

        [asxv5pxx\asst8]
              st8adr.c
              st8pst.c
              st8mch.c
              st8.h
              st8gbl.asm
              st8err.asm
              st8err.bat
              tst8.asm
              tst8.bat

        [asxv5pxx\ascheck\end]
              build.bat
              tbuild.bat
              t01.asm
              t02.asm
              t03.asm
              t04.asm
              t05.asm
              t06.asm
              t07.asm
              t08.asm

        [asxv5pxx\aspic\ptoa]
              pictoasx.c
              ptoa.h

        [asxv5pxx\asxxsrc]
              aslex.c
              aslist.c
              asmain.c
              asmcro.c
              asout.c
              asexpr.c
              asdata.c
              assubr.c
              asxxxx.h

        [asxv5pxx\linksrc]
              aslink.h
              lkdata.c
              lklex.c
              lklist.c
              lkmain.c
              lkbank.c
              lksym.c
              lkrloc3.c
              lkrloc4.c

        [asxv5pxx\s19os9]
              a19os9.c

        [asxv5pxx\asxmak\cygwin\build]
              makefile

        [asxv5pxx\asxmak\cygwin\misc]
              tstwtee.bat
              tstscn.bat

        [asxv5pxx\asxmak\djgpp\build]
              makefile
              build.bat

        [asxv5pxx\asxmak\djgpp\misc]
              tstwtee.bat
              tstscn.bat

        [asxv5pxx\asxmak\linux\build]
              makefile

        [asxv5pxx\asxmak\linux\misc]
              tstscn.bat

        [asxv5pxx\asxmak\symantec\build]
              make.bat
              asst6.def
              asst6.dpd
              asst6.lnk
              asst6.mak
              asst6.opn
              asst6.prj
              asst7.def
              asst7.dpd
              asst7.lnk
              asst7.mak
              asst7.opn
              asst7.prj
              asst8.def
              asst8.dpd
              asst8.lnk
              asst8.mak
              asst8.opn
              asst8.prj

        [asxv5pxx\asxmak\symantec\misc]
              tstwtee.bat
              tstscn.bat

        [asxv5pxx\asxmak\turboc30\build]
              makefile
              asst6.dsk
              asst6.prj
              asst7.dsk
              asst7.prj
              asst8.dsk
              asst8.prj

        [asxv5pxx\asxmak\turboc30\misc]
              tstwtee.bat
              tstscn.bat

        [asxv5pxx\asxmak\vc6\build]
              _prep.bat
              make.bat
              \asst6\asst6.dsp
              \asst6\asst6.dsw
              \asst6\asst6.ncb
              \asst6\release\
              \asst7\asst7.dsp
              \asst7\asst7.dsw
              \asst7\asst7.ncb
              \asst7\release\
              \asst8\asst8.dsp
              \asst8\asst8.dsw
              \asst8\asst8.ncb
              \asst8\release\

        [asxv5pxx\asxmak\vc6\misc]
              tstwtee.bat
              tstscn.bat

        [asxv5pxx\asxmak\vs05\build]
              _prep.bat
              make.bat
              \asst6\asst6.vcproj
              \asst6\asst6.sln
              \asst6\asst6.suo
              \asst7\asst7.vcproj
              \asst7\asst7.sln
              \asst7\asst7.suo
              \asst8\asst8.vcproj
              \asst8\asst8.sln
              \asst8\asst8.suo

        [asxv5pxx\asxmak\vs05\misc]
              tstwtee.bat
              tstscn.bat

        [asxv5pxx\asxmak\watcom\build]
              make.bat
              asst6.lk1
              asst6.mk
              asst6.mk1
              asst6.tgt
              asst6.wpj
              asst7.lk1
              asst7.mk
              asst7.mk1
              asst7.tgt
              asst7.wpj
              asst8.lk1
              asst8.mk
              asst8.mk1
              asst8.tgt
              asst8.wpj

        [asxv5pxx\asxmak\watcom\misc]
              tstwtee.bat
              tstscn.bat

        [asxv5pxx\asxxmisc]
              asxxscan.c

        [asxv5pxx\asxhtml]
              asxxxx.htm
              asmlnk.htm
              asmlnk.txt
              asmdoc.htm
              asxbug.htm
              asxupd.htm
              asst6.htm
              asst7.htm
              asst8.htm


   You  must recompile the ASxxxx Assemblers, Linker, and Utili-
ties to incorporate the updates.  The resulting version will  be
V05.03 .
